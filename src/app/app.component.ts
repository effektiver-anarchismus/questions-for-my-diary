import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { HelloService } from './hello.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.styl'],
})
export class AppComponent {
  nameForm = new FormGroup({
    name: new FormControl(''),
  });
  greeting: string = '';

  constructor(private helloService: HelloService) {}

  onSubmit() {
    this.helloService
      .hello(this.nameForm.get('name')?.value)
      .subscribe((res) => (this.greeting = res));
  }
}
