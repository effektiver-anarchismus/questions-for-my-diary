import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class HelloService {

  constructor(private http: HttpClient) { }

  public hello(name: string): Observable<string> {
    return this.http.post<string>('/.netlify/functions/hello', { name });
  }
}
