exports.handler = (event, context, callback) => {
  const data = JSON.parse(event.body);
  console.log("Function `hello` invoked: ", data);
  console.log("Context: ", context);

  return callback(null, {
    statusCode: 200,
    body: JSON.stringify(`Hello ${data.name}!`),
  });
};
